<?php

/**
 * Clase modelo que permite mejorar la obtención de datos
 */

class Cibase extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/**
     * Devuelve un array con la informaci‚àö‚â•n de una consulta
     * 
     * @param string $query Consulta SQL
     * @return array filas y columnas con la informaci‚àö‚â•n de la consulta
    */
    function get_array($query)
    {
    	$array = null;
        $i = 0;
        foreach($query->result() as $row)
        {
            $array[$i] = $row;
            $i++;
        }
        return $array;
    }
        
    /**
     * Devuelve un row con la informaci‚àö‚â•n de una consulta
     * 
     * @param string $query Consulta SQL
     * @return Object fila con la informaci‚àö‚â•n de la consulta
     */
    function get_row($query)
    {
        $row = null;
        if($query->num_rows() > 0)
        {
            $row = $query->row();
        }
        return $row;
    }
}

/* Fin del archivo: cibase.php */
/* Ubicación: ./application/models/cibase.php */